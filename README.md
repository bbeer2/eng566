# README #

### What is this repository for? ###

ENG566 final project

### How do I get set up? ###

1) Install the following packages:
Python 2.7/3.0
Flask (http://flask.pocoo.org)
* pip install Flask
WTForms (http://wtforms.readthedocs.io/en/latest/)
* pip install wtforms

2) Clone this repo

3) Start flask app
#FLASK_APP=main_app.py flask run
You should see output like:
 * Serving Flask app "school.eng566_project.main_app"
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)

4) Open browser
Try navigating to the default address, whatever is listed as the output from above (http://127.0.0.1:5000/), try navigating to this page and the app should be up and running!
If there are issues here please see flask documenation for debugging


### Who do I talk to? ###

Brian Beer