import datetime

class model():
    def __init__(self):
        self.employees_data = []
        self.customers_data = []
        self.vendors_data = []
        self.payroll_data = payroll()
        self.invoice_history = invoices()
        self.purchase_order_history = purchase_orders()
        self.sale_price_per_unit = 2.50  # Fixed per the example problem
        self.date = current_date()

        self.income_data = income_statement(sales=0,
                                            cogs=0,
                                            payroll=0,
                                            payroll_withholding=0,
                                            bills=0,
                                            other_income=0,
                                            income_taxes=0)
        self.balance_sheet_data = balance_sheet(cash=200000,
                                                ar=0,
                                                inventory_data=inventory(0),
                                                lands_buildings=0,
                                                equipment=0,
                                                furniture_fixtures=0,
                                                ap=0,
                                                notes_payable=0,
                                                accruals=0,
                                                mortgage=0)
        self.employees_data.append(
            employee('john', 'smith', '123 front st', '', 'San Francisco', 'CA', '12345', '123-45-6789', '0', '150000'))
        self.customers_data.append(customer('Smith Co.', '', '', '', '', '', '', '', '2.50'))
        self.customers_data.append(customer('Toys r Us', '', '', '', '', '', '', '', '2.50'))
        self.customers_data.append(customer('Online Hobbies', '', '', '', '', '', '', '', '2.50'))
        self.customers_data.append(customer('TowerHobbies', '', '', '', '', '', '', '', '2.50'))
        self.customers_data.append(customer('Hobbytron', '', '', '', '', '', '', '', '2.50'))
        self.customers_data.append(customer('Walmart', '', '', '', '', '', '', '', '2.50'))
        self.customers_data.append(customer('Kmart', '', '', '', '', '', '', '', '2.50'))
        self.customers_data.append(customer('Target', '', '', '', '', '', '', '', '2.50'))
        self.customers_data.append(customer('Hobbytown', '', '', '', '', '', '', '', '2.50'))
        self.customers_data.append(customer('Cars are Us', '', '', '', '', '', '', '', '2.50'))
        self.vendors_data.append(vendor('Tires R Us', 'Tires', '0.02', '', '', '', '', ''))
        self.vendors_data.append(vendor('Mr Mixers', 'Mixer', '0.05', '', '', '', '', ''))
        self.vendors_data.append(vendor('Casting Man ', 'Body Casting', '0.10', '', '', '', '', ''))
        self.vendors_data.append(vendor('Castings for U', 'Cab Casting', '0.05', '', '', '', '', ''))
        self.vendors_data.append(vendor('Acme Plastics', 'Windshield', '0.10', '', '', '', '', ''))
        self.vendors_data.append(vendor('Micro Axles', 'Axles', '0.01', '', '', '', '', ''))
        self.vendors_data.append(vendor('All Rims', 'Rims', '0.01', '', '', '', '', ''))
        self.vendors_data.append(vendor('Screw it', 'Screw', '0.02', '', '', '', '', ''))
        self.vendors_data.append(vendor('Many Boxes', 'Box', '0.05', '', '', '', '', ''))
        self.vendors_data.append(vendor('None', 'Labor', '0.10', '', '', '', '', ''))
        self.vendors_data.append(vendor('Mast Packing', 'Master Pack', '0.02', '', '', '', '', ''))

    def reinitialize(self):
        self.__init__()

class current_date():
    def __init__(self):
        self._date = datetime.date.today()
        self.months_advanced = 0

    def advance_a_month(self):
        self.months_advanced += 1

    @property
    def date(self):
        delta = datetime.timedelta(self.months_advanced * 365 / 12)
        return (datetime.date.today() + delta).isoformat()

class employee():
    def __init__(self, firstname, lastname, address1, address2,
                 city, state, zipcode, ssn, witholdings, salary):
        self.firstname = firstname
        self.lastname = lastname
        self.address1 = address1
        self.address2 = address2
        self.city = city
        self.state = state
        self.zipcode = zipcode
        self.ssn = ssn
        self.witholdings = int(witholdings)
        self.salary = float(salary)

class customer():
    def __init__(self, companyname, firstname, lastname, address1, address2,
                 city, state, zipcode, price):
        self.companyname = companyname
        self.firstname = firstname
        self.lastname = lastname
        self.address1 = address1
        self.address2 = address2
        self.city = city
        self.state = state
        self.zipcode = zipcode
        self.price = float(price)

class vendor():
    def __init__(self, companyname, part, price_per_unit, address1, address2, city,
                 state, zipcode):
        self.companyname = companyname
        self.part = part
        self.price_per_unit = float(price_per_unit)
        self.address1 = address1
        self.address2 = address2
        self.city = city
        self.state = state
        self.zipcode = zipcode

class payroll():
    def __init__(self, todal_dispursement=0, total_witholdings=0):
        self.payroll_events = []
        self.total_dispursement = todal_dispursement
        self.total_witholdings = total_witholdings

    def add_payroll_event(self, event):
        self.payroll_events.append(event)
        self.total_dispursement += event.dispursement
        self.total_witholdings += event.witholdings
        return self.total_dispursement + self.total_witholdings

class payroll_event():
    def __init__(self, employee, date_paid, bounce=0):
        self.lastname = employee.lastname
        self.date_paid = date_paid
        self.bounce = bounce
        self.salary = employee.salary/12.0
        self.federal_tax = 1559.45 + (self.salary - 7850) * 0.28
        self.state_tax = self.salary * 0.0495
        self.social_sec_tax = self.salary * 0.062
        self.medicare_tax = self.salary * 0.0145
        self.dispursement = self.salary - self.witholdings

    @property
    def witholdings(self):
        return self.federal_tax + self.state_tax + self.social_sec_tax + self.medicare_tax

class part():
    def __init__(self, price_per_unit, quantity, bom_qty):
        self.price_per_unit = float(price_per_unit)
        self.quantity = int(quantity)
        self.bom_qty = int(bom_qty)

    @property
    def value(self):
        return self.price_per_unit * self.quantity

    @property
    def re_order(self):
        if self.quantity < 10000:
            return 'X'
        return ''

class inventory():
    def __init__(self, complete_units):
        self.parts = {}
        self.parts['Tires'] = part('0.02', '0', 8)
        self.parts['Mixer'] = part('0.05', '0', 1)
        self.parts['Body Casting'] = part('0.10', '0', 1)
        self.parts['Cab Casting'] = part('0.05', '0', 1)
        self.parts['Windshield'] = part('0.10', '0', 1)
        self.parts['Axles'] = part('0.01', '0', 4)
        self.parts['Rims'] = part('0.01', '0', 8)
        self.parts['Screw'] = part('0.02', '0', 1)
        self.parts['Box'] = part('0.05', '0', 1)
        self.parts['Labor'] = part('0.10', '0', 1)
        self.parts['Master Pack'] = part('0.02', '0', 1)
        self.complete_units = complete_units

    def purchase_part(self, part_name, additional_qty):
        self.parts[part_name].quantity += additional_qty

    def purchase_complete_unit(self, qty):
        self.complete_units -= qty

    @property
    def cog_per_unit(self):
        count = 0
        for name, p in self.parts.iteritems():
            count += p.price_per_unit * p.bom_qty
        return count

    @property
    def total(self):
        count = 0
        for name, p in self.parts.iteritems():
            count += p.value
        return count

    @property
    def complete_units_to_build(self):
        units_possible = [p.quantity/p.bom_qty for name, p in self.parts.iteritems()]
        if units_possible:
            return min(units_possible)
        else:
            return 0

    @property
    def complete_units_cost(self):
        return self.complete_units * self.cog_per_unit

class purchase_orders():
    def __init__(self):
        self.po_events = []
        self.po_number = 1

    def create_po(self, supplier, part, quantity, price_per_unit, total, date_paid):
        self.po_events.append(purchase_order(self.po_number, supplier, part, quantity, price_per_unit, total, date_paid))
        self.po_number += 1

class purchase_order():
    def __init__(self, number, supplier, part, quantity, price_per_unit, total, date_paid):
        self.number = number
        self.supplier = supplier
        self.part = part
        self.quantity = quantity
        self.price_per_unit = price_per_unit
        self.total = total
        self.date_paid = date_paid

class invoices():
    def __init__(self):
        self.invoice_events = []
        self.invoice_number = 1

    def create_invoice(self, customer, quantity, price_per_unit, total, date_paid):
        self.invoice_events.append(invoice(self.invoice_number, customer, quantity, price_per_unit, total, date_paid))
        self.invoice_number += 1

class invoice():
    def __init__(self, number, customer, quantity, price_per_unit, total, date_paid):
        self.number = number
        self.customer = customer
        self.quantity = quantity
        self.price_per_unit = price_per_unit
        self.total = total
        self.date_paid = date_paid

class income_statement():
    def __init__(self, sales, cogs, payroll, payroll_withholding, bills,
                 other_income, income_taxes):
        self.sales = sales
        self.cogs = cogs
        self.payroll = payroll
        self.payroll_withholding = payroll_withholding
        self.bills = bills
        self.expense_accounts = monthly_expenses(maintenance=1250*2,
                                                 cleaning=416.67*2,
                                                 water=416.67*2,
                                                 sewage=833.33*2,
                                                 electricity=1250*2,
                                                 travel=833.33*2,
                                                 donuts=416.67*2,
                                                 gas=1250*2,
                                                 internet=833.33*2,
                                                 phone=833.33*2)
        self.other_income = other_income
        self.income_taxes = income_taxes

    @property
    def annual_expenses(self):
        return self.expense_accounts.total_expenses

    @property
    def gross_profit(self):
        return self.sales - self.cogs

    @property
    def expenses(self):
        return self.payroll + self.bills + self.annual_expenses

    @property
    def operating_income(self):
        return self.gross_profit - self.expenses

    @property
    def net_income(self):
        return self.operating_income - self.income_taxes

class balance_sheet():
    def __init__(self, cash, ar, inventory_data, lands_buildings, equipment, furniture_fixtures,
                 ap, notes_payable, accruals, mortgage):
        self.cash = cash
        self.ar = ar
        self.lands_buildings = lands_buildings
        self.equipment = equipment
        self.furniture_fixtures = furniture_fixtures
        self.ap = ap
        self.notes_payable = notes_payable
        self.accruals = accruals
        self.mortgage = mortgage
        self.inventory_data = inventory_data

    @property
    def total_current_assets(self):
        return self.cash + self.ar + self.inventory_data.total

    @property
    def total_fixed_assets(self):
        return self.lands_buildings + self.equipment + self.furniture_fixtures

    @property
    def total_assets(self):
        return self.total_current_assets + self.total_fixed_assets

    @property
    def total_current_liabilities(self):
        return self.ap + self.notes_payable + self.accruals

    @property
    def total_long_term_debt(self):
        return self.mortgage

    @property
    def total_liabilities(self):
        return self.total_current_liabilities + self.total_long_term_debt

    @property
    def net_worth(self):
        return self.total_assets - self.total_liabilities

class monthly_expenses():
    def __init__(self, maintenance, cleaning, water, sewage, electricity, travel, donuts, gas, internet, phone):
        self.maintenance = maintenance
        self.cleaning = cleaning
        self.water = water
        self.sewage = sewage
        self.electricity = electricity
        self.travel = travel
        self.donuts = donuts
        self.gas = gas
        self.internet = internet
        self.phone = phone
        self.total_expenses = self.total_monthly_expenses

    @property
    def total_monthly_expenses(self):
        return self.maintenance + self.cleaning + self.water + self.sewage + self.electricity + self.travel + self.donuts + self.gas + self.internet + self.phone